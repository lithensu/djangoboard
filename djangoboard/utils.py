from django.conf import settings
from django.http import HttpRequest
from django.shortcuts import redirect
from django.urls import reverse

from djangoboard.models import IP


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return IP.objects.get_or_create(address=ip)[0]


def human_required(view_function):
    def wrapper(request: HttpRequest, *args, **kwargs):
        if request.session.get('human', False) or not settings.DJANGOBOARD_REQUIRE_CAPTCHA:
            return view_function(request, *args, **kwargs)
        else:
            return redirect(reverse('djangoboard:captcha'))
    return wrapper